<?php
include('inc/mysql.php');
include('inc/filter.php');
include('inc/DB.php');
include('inc/functions.php');

//-------------------------------------------------------
//DEFAULT
$template='templates/t_default.php';
$javascript='';
//-------------------------------------------------------

if( ! $_GET['do']){$_GET['do']='main';}
if($_GET['do']=='main'){include('pages/p_main.php');$template='templates/t_main.php';}
if($_GET['do']=='logout'){include('pages/p_logout.php');$template='templates/t_main.php';}
if($_GET['do']=='profile'){include('pages/p_profile.php');$template='templates/t_main.php';}
if($_GET['do']=='profile_advertisements'){include('pages/p_profile_advertisements.php');$template='templates/t_main.php';}
if($_GET['do']=='add_advertisement'){include('pages/p_add_advertisement.php');$template='templates/t_main.php';}
if($_GET['do']=='edit_advertisement'){include('pages/p_edit_advertisement.php');$template='templates/t_main.php';}
if($_GET['do']=='list_advertisements'){include('pages/p_list_advertisements.php');$template='templates/t_main.php';}
if($_GET['do']=='one_advertisement'){include('pages/p_one_advertisement.php');$template='templates/t_main.php';}

include($template);
?>