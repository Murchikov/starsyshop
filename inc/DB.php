<?php
	class DB
	{
		var $table = ''; //название таблицы
		var $where = ''; //условие выборки
		var $order = ''; //сортировка
		var $limit = ''; //лимит выборки
		var $what = ''; //то, что выбирается
		var $result = NULL; //результат
		var $row=null; //строка полученная
        var $fields=null; //поля
        var $query='';//запрос
        var $N=null; //число строк
		
		//конструктор
		function DB($table_name, $what='*')
		{
			$this->table=$table_name;
            $this->what=$what;
            $r=mysql_query("SHOW columns FROM ".$this->table);
            $counter=0;
            while($row=mysql_fetch_array($r))
			{
                $this->fields[$counter]=$row[0];
            	$counter++;
            }
		}
		
		//функция поиска (готовит запрос к БД и выполнет его)
		function find()
		{
			$this->query = "SELECT {$this->what} FROM ".$this->table." ";
			if(strlen($this->where) > 0)
			{
            	$this->query.="where ".$this->where." ";
			}
			if(strlen($this->order) > 0)
			{
            	$this->query.="order by ".$this->order." ";
			}
			if(strlen($this->limit) > 0)
			{
            	$this->query.="limit ".$this->limit." ";
			}
			if(!$this->result=mysql_query($this->query)){
            	echo "Ошибка MySQL! (".$this->query.")<br>";
            	echo mysql_error();
			}
			$this->N=mysql_num_rows($this->result);
		}
		
		//Фетчит запрос, подготовленный в find()
		function fetch()
		{
        	if($this->row=mysql_fetch_array($this->result))
			{
            	foreach($this->row as $k=>$v)
				{
            		if(!is_numeric($k))
					{
                        eval('$this->'.$k.'=$v;');
            		}
            	}
            	return true;
        	}
			else
			{
            	return false;
        	}
		}
		
		//добавление записей в таблицу
		function insert()
		{
            $this->query="INSERT INTO `".$this->table."` (";
            $comma=false;
        	foreach($this->fields as $field_name)
			{
                if($comma)
				{
            		$this->query.=", ";
            	}
            	$this->query.='`'.$field_name.'`';
            	$comma=true;
        	}
        	$this->query.=") values(";
        	$comma=false;
        	foreach($this->fields as $field_name)
			{
                if($comma)
				{
            		$this->query.=", ";
            	}
            	eval ('$this->query.="\'".mysql_real_escape_string(stripslashes($this->'.$field_name.'))."\'";');
            	$comma=true;
        	}
        	$this->query.=")";
        	if(!mysql_query($this->query))
			{
            	echo "Ошибка MySQL!";
            	echo mysql_error();
        	}
		}
		
		//обновление записей
		function update()
		{
            $this->query="update ".$this->table." set ";
        	$comma=false;
        	foreach($this->fields as $field_name)
			{
                if($field_name!='id'){
	                eval('  if(isset($this->'.$field_name.'))
								{
									if($comma)
									{
										$this->query.=", ";
									}
									$comma=true;
									$this->query.="`'.$field_name.'`=\'".mysql_real_escape_string(stripslashes($this->'.$field_name.'))."\'";
								}'
						);
	            }

        	}
        	if(trim($this->where)=='' && $this->id>0)
			{
            	$this->where='id=\''.$this->id.'\'';
        	}
        	$this->query.=" where ".$this->where;
        	if(!mysql_query($this->query))
			{
            	echo "Ошибка MySQL! (".$this->query.")<br>";
            	echo mysql_error();
        	}
		}
		
		//удаление записи
		function delete()
		{
        	$this->query="delete from ".$this->table;
        	if(trim($this->where)=='' && $this->id>0)
			{
            	$this->where='id=\''.$this->id.'\'';
        	}
        	if(strlen($this->where)>0)
			{
            	$this->query.=' where '.$this->where;
        	}
        	if(!mysql_query($this->query))
			{
            	echo "Ошибка MySQL! (".$this->query.")<br>";
            	echo mysql_error();
        	}
		}
		
		//получение последнего id
		function last_id()
		{
        	$this->result=mysql_query("SELECT LAST_INSERT_ID() id;");
            $this->fetch();
            return $this->id;
        }
		
		//получение записи по id
		function get($id)
		{
            $id = mysql_real_escape_string($id);
            if (empty($this->where))
			{
                $this->where="id='{$id}'";
			}
            else
			{	
				$this->where.=" AND id='{$id}'";
			}	
            $this->limit="1";
        	$this->find();
        	$this->fetch();
		}
		
		//Выполняется ли запрос - это надо для функции queryval()
		function query($query)
		{
        	$this->query=$query;
        	if($this->result=mysql_query($query)) //если выполняется, то true
			{
        		$this->N=mysql_num_rows($this->result);
                return true;
            }else{
                return false;
            }
        }
		
		//Функция делает выборку по запросу, написанному самому, а не при помощи переменных класса
		function queryval($query)
		{
        	if($this->query($query))
			{
            	$this->fetch();
        	}
        }
		
		//типа деструктора
		function clear()
		{
        	foreach($this->fields as $field_name)
			{
            	eval('unset($this->'.$field_name.');');
            }
		}

	}
?>