<?php

if(count($_POST))
{
	if($_POST['name'] != '')
	{
		mysql_query("INSERT INTO `advertisement` 
					(`name_adv`, `category`, `description`, `price`, `date`, `user`, `city`, `closed`, `accept`)
					VALUES ('".$_POST['name']."', ".(int)$_POST['category'].", '".$_POST['description']."', '".$_POST['price']."', CURDATE(), ".(int)$_COOKIE['user_id'].", ".(int)$_POST['city'].", 0, 0)");
		if(mysql_insert_id() > 0)
		{
			$id_adv = mysql_insert_id();
			for($i = 1; $i < 7; $i ++)
			{
				if(!empty($_FILES['photo_'.$i]['tmp_name']))
				{
					$dest_1 = '';
					$dest_2 = '';
					$dest_3 = '';
					$valid_types = array("gif", "jpg", "png", "jpeg");
					if(is_uploaded_file($_FILES['photo_'.$i]['tmp_name']))
					{
						$type = substr($_FILES['photo_'.$i]['name'], 1 + strrpos($_FILES['photo_'.$i]['name'], "."));
						if(in_array($type, $valid_types))
						{
							if(move_uploaded_file($_FILES['photo_'.$i]['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/images/advertisements/original/'.$id_adv.'_'.$i.'.'.$type))
							{
								$dest_1 = '/images/advertisements/220x220/'.$id_adv.'_220x220_'.$i.'.'.$type;
								$dest_2 = '/images/advertisements/150x150/'.$id_adv.'_150x150_'.$i.'.'.$type;
								$dest_3 = '/images/advertisements/960x720/'.$id_adv.'_960x720_'.$i.'.'.$type;
								$res_1 = img_resize($_SERVER['DOCUMENT_ROOT'].'/images/advertisements/original/'.$id_adv.'_'.$i.'.'.$type, $_SERVER['DOCUMENT_ROOT'].$dest_1, 220, 220);
								$res_2 = img_resize($_SERVER['DOCUMENT_ROOT'].'/images/advertisements/original/'.$id_adv.'_'.$i.'.'.$type, $_SERVER['DOCUMENT_ROOT'].$dest_2, 150, 150);
								$res_3 = img_resize($_SERVER['DOCUMENT_ROOT'].'/images/advertisements/original/'.$id_adv.'_'.$i.'.'.$type, $_SERVER['DOCUMENT_ROOT'].$dest_3, 960, 720);
								
								mysql_query("INSERT INTO `photo_adverts` (`advert_id`, `path`, `size`, `number`) VALUES (".$id_adv.", '".$dest_1."', '220x220', ".$i.")");
								if(mysql_insert_id() > 0)
								{
									mysql_query("INSERT INTO `photo_adverts` (`advert_id`, `path`, `size`, `number`) VALUES (".$id_adv.", '".$dest_2."', '150x150', ".$i.")");
									if(mysql_insert_id() > 0)
									{
										mysql_query("INSERT INTO `photo_adverts` (`advert_id`, `path`, `size`, `number`) VALUES (".$id_adv.", '".$dest_3."', '960x720', ".$i.")");
										if(mysql_insert_id() > 0)
										{
											continue;
										}
										else
										{
											echo '<script> alert("Ошибка при загрузке фото"); </script>';
										}
									}
									else
									{
										echo '<script> alert("Ошибка при загрузке фото"); </script>';
									}
								}
								else
								{
									echo '<script> alert("Ошибка при загрузке фото"); </script>';
								}
							}
							else
							{
								echo '<script> alert("Ошибка при загрузке фото") </script>';
							}
						}
						else
						{
							echo '<script> alert("Неверный тип данных фото") </script>';
						}
					}
				}
			}
			header("Location: /?do=profile_advertisements");
			exit();
			
		}
		else
		{
			echo '
				<script>
					alert("Ошибка введенных данных");
				</script>
			';
		}
	}
}

$title = 'Добавление объявления';

$categories = mysql_query("SELECT * FROM `category`");
$cities = mysql_query("SELECT * FROM `cities` ORDER BY `city_id`");

$content = '
	<div class="profile_advert">
		<h3>Новое объявление</h3>
		<br>
		<br>
		
		<form method="POST" action="" enctype="multipart/form-data">
		<table class="profile_table">
			<tr>
				<td><b>Название</b></td>
				<td><input style="width: 423px; height: 35px; font-size: 20px;" type="text" name="name"/></td>
			</tr>	
			<tr>
				<td><b>Категория</b></td>
				<td>
					<select name="category" style="width: 423px; height: 35px; font-size: 20px;">
						<option disabled>Выберите категорию</option>
';						
					while($category = mysql_fetch_assoc($categories))
					{
$content.='
						<option value="'.$category['id'].'">'.$category['name'].'</option>
';						
					}
$content.='			</select>
				</td>
			</tr>	
			<tr>
				<td><b>Описание</b></td>
				<td><textarea style="font-size: 17px;" name="description" rows="10" cols="40"></textarea></td>
			</tr>
			<tr>
				<td><b>Цена</b></td>
				<td><input type="text" name="price" value="0" style="width: 200px; height: 35px; font-size: 20px;"/> Руб.</td>
			</tr>
			<tr>
				<td><b>Город</b></td>
				<td>
					<select name="city" style="width: 423px; height: 35px; font-size: 20px;">
						<option disabled>Выберите город</option>
';						
					while($city = mysql_fetch_assoc($cities))
					{
$content.='
						<option value="'.$city['city_id'].'">'.$city['name'].'</option>
';						
					}
$content.='			</select>
				</td>
			</tr>
			<tr>
				<td><b>Фото 1 <small>(основное фото)</small></b></td>
				<td><input type="file" name="photo_1"/></td>
			</tr>
			<tr>
				<td><b>Фото 2</b></td>
				<td><input type="file" name="photo_2"/></td>
			</tr>
			<tr>
				<td><b>Фото 3</b></td>
				<td><input type="file" name="photo_3"/></td>
			</tr>
			<tr>
				<td><b>Фото 4</b></td>
				<td><input type="file" name="photo_4"/></td>
			</tr>
			<tr>
				<td><b>Фото 5</b></td>
				<td><input type="file" name="photo_5"/></td>
			</tr>
			<tr>
				<td><b>Фото 6</b></td>
				<td><input type="file" name="photo_6"/></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Добавить" class="button1" /></td>
			</tr>
		</table>	
		</form>
';


?>