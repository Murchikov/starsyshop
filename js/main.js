function ajax_show(url,w,h)
{
    $("#ajax_wrapper").width(w);
    $("#ajax_wrapper").height(h);
    $("#ajax_wrapper").css("margin-left","-" + w/2 + "px");
    $("#ajax_wrapper").css("margin-top","-" + h/2 + "px");
    $("#ajax_body").load(url, function (html){$("#ajax_wrapper").fadeIn("fast")});
}

function ajax_hide()
{
	$("#ajax_wrapper").fadeOut("fast");
}