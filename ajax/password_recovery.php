<?php
/* Функция для отправки почты
 * Почта должна отправляться в UTF-8,
 * тогда её корректно видят все почтовые клиенты.
 */
function html_mail($to, $subject = '(No subject)', $message = '', $from='robot@starsyshop.ru') 
{
	$header = 'MIME-Version: 1.0' . "\n" . 'Content-type: text/html; charset=UTF-8' . "\n" . 'From: <' . $from . ">\n";
	if (mail($to, '=?UTF-8?B?'.base64_encode($subject).'?=', $message, $header))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/* функция генерации рандомной строки */

function random_string($length, $chartypes) 
{
    $chartypes_array=explode(",", $chartypes);
    // задаем строки символов. 
    //Здесь вы можете редактировать наборы символов при необходимости
    $lower = 'abcdefghijklmnopqrstuvwxyz'; // lowercase
    $upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; // uppercase
    $numbers = '1234567890'; // numbers
    $special = '^@*+-+%()!?'; //special characters
    $chars = "";
    // определяем на основе полученных параметров, 
    //из чего будет сгенерирована наша строка.
    if (in_array('all', $chartypes_array)) {
        $chars = $lower . $upper. $numbers . $special;
    } else {
        if(in_array('lower', $chartypes_array))
            $chars = $lower;
        if(in_array('upper', $chartypes_array))
            $chars .= $upper;
        if(in_array('numbers', $chartypes_array))
            $chars .= $numbers;
        if(in_array('special', $chartypes_array))
            $chars .= $special;
    }
    // длина строки с символами
    $chars_length = strlen($chars) - 1;
    // создаем нашу строку,
    //извлекаем из строки $chars символ со случайным 
    //номером от 0 до длины самой строки
    $string = $chars{rand(0, $chars_length)};
    // генерируем нашу строку
    for ($i = 1; $i < $length; $i = strlen($string)) {
        // выбираем случайный элемент из строки с допустимыми символами
        $random = $chars{rand(0, $chars_length)};
        // убеждаемся в том, что два символа не будут идти подряд
        if ($random != $string{$i - 1}) $string .= $random;
    }
    // возвращаем результат
    return $string;
}


if(count($_POST))
{
	if( $_POST['email'] != '')
	{
		//проверка существования указанной почты в системе
		$query = mysql_query("SELECT * FROM `users` WHERE `mail`='".$_POST['email']."' LIMIT 1");
		if($query)
		{
			$data = mysql_fetch_assoc($query);
			//генерация нового пароля из 8 символов
			$length = 8;
			$chartypes = "lower,upper,numbers";
			$new_password = random_string($length, $chartypes);
			
			//отправка сообщения на почту
			$content_mail = array();
			$content_mail[] = '
				<p>Здравствуйте</p>
				<p>Вас приветствует сервис <a href="http://starsyshop.ru">STARSYSHOP</a></p>
				<p>Вам пришло данное письмо, т.к. Вы воспользовались функцией восстановления пароля</p>
				<p>Новый пароль для входа в систему: <b>'.$new_password.'</b></p>
				<br>
				<br>
				<b>Рекомендуем изменить данный пароль при первом входе в систему</b>
				
			';
			if(html_mail($_POST['regemail'],'Восстановление пароля starsyshop.ru',implode(PHP_EOL,$content_mail)))
			{
				//смена пароля в БД
				$sql = mysql_query("UPDATE `users` SET `password`='".md5($new_password)."' WHERE `id`=".$data['id']);
				if($sql)
				{
					echo '<font color="green">На указанную почту выслан новый пароль. Рекомендуем изменить его при входе в систему.</font>';
				}
				else
				{
					echo '<font color="red">Произошла ошибка</font>';
				}
			}
			else
			{
				echo '<font color="red">Произошла ошибка при отправке письма</font>';
			}
		}
		else
		{
			echo '<font color="red">Вы ввели неверную почту. Пользователя с такой почтой нет в системе.</font>';
		}
	}
}

?>