<script type="text/javascript">
	jQuery("#form").submit(function(){
		jQuery.ajax({
			url: "/ajax.php?s=change_pass",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data)
			{
				jQuery("#message").html(data);
			},
			error: function() 
			{
			}
		});	
		return false;
	});
</script>

<div id="ajax_title">Изменение пароля<br><br></div>
<form id="form" method="POST">
	<div style="margin-left: 48px;">
		<table>
			<tr>
				<td>
					<input type="text" name="old_pass" style="width:300px; padding-left: 9px;" placeholder="Старый пароль"><br><br>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" name="new_pass" style="width:300px; padding-left: 9px;" placeholder="Новый пароль" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Не менее восьми символов, содержащих хотя бы одну цифру и символы из верхнего и нижнего регистра"><br><br>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" name="new_pass2" style="width:300px; padding-left: 9px;" placeholder="Повторите пароль"><br><br>
				</td>
			</tr>
			<tr>
				<td>
					<input class="button1" type="submit" value="Изменить">
					<br>
				</td>
			</tr>
			<tr>
				<td>
					<div style="width:300px;" id="message">
					</div>
				</td>
			</tr>
		</table>
	</div>
</form>

