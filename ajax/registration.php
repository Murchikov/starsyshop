<?php

if(count($_POST))
{
    $reg=true;
    $message='';

	//обработка фамилии и имени
	if(isset($_POST['last_name']) AND $_POST['last_name'] == '')
	{
		$message = '<font color="red">Поле "Фамилия" является обязательным</font>';
		$reg = false;
	}
	if(isset($_POST['first_name']) AND $_POST['first_name'] == '')
	{
		$message = '<font color="red">Поле "Имя" является обязательным</font>';
		$reg = false;
	}
	
//обработка почты пользователя

    if(isset($_POST['email']))
    {
        if($_POST['email']!='' AND !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
        {
            $message = '<font color="red">Введенная почта некорректна.</font>';
            $reg=false;
        }
    }
    else
    {
        $_POST['email'] = '';
        $reg = false;
    }
//обработка пароля пользователя
    if(isset($_POST['password']) AND isset($_POST['password_2']))
    {
        if(strlen($_POST['password']) > 7)
        {
            if($_POST['password'] != $_POST['password_2'])
            {
                $message .= '<br><font color="red">Пароли не совпадают</font>';
                $reg=false;
            }
        }
        else
        {
            $message .= '<br><font color="red">Пароль должен быть не короче 8-и символов</font>';
            $reg=false;
        }
    }
    else
    {
        $reg = false;
    }

//Если не произошло ошибок, то добавляем пользователя
    if($reg)
    {
        //проверка уникальности почты
		$check_query = mysql_query("SELECT `id` FROM `users` WHERE `mail`='".$_POST['email']."'");
		if($check_query != FALSE)
		{
			$res = mysql_fetch_assoc($check_query);
			if($res['id'] > 0)
			{
				$message = '<font color="red">Пользователь с такой почтой уже есть</font>';
			}
			else
			{
				//добавление пользователя
				$password = md5($_POST['password']);
				$now = time();
				mysql_query("INSERT INTO `users` (`password`, `mail`, `registration_date`, `first_name`, `last_name`) 
								VALUES ('".$password."', '".$_POST['email']."', ".$now.", '".$_POST['first_name']."', '".$_POST['last_name']."')");
				$last_insert_id = mysql_insert_id();
				if($last_insert_id > 0)
				{
					$message = '<font color="green">Регистрация прошла успешно</font>';
				}
				else
				{
					$message = '<font color="red">Ошибка при регистрации</font>';
				}
			}
		}
    }
    echo $message;
    exit();
}

?>