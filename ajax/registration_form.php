<script type="text/javascript">
	jQuery("#form").submit(function(){
		jQuery.ajax({
			url: "/ajax.php?s=registration",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data)
			{
				jQuery("#message").html(data);
			},
			error: function() 
			{
			}
		});	
		return false;
	});
</script>

<div id="ajax_title">Регистрация<br><br></div>
<form id="form" method="POST">
	<div style="margin-left: 90px;">
		<table>
			<tr>
				<td>
					<input type="text" name="email" style="width:300px; padding-left: 9px;" placeholder="Электронная почта" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" title="Пример: example@mail.com" required /><br><br>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" name="last_name" style="width:300px; padding-left: 9px;" placeholder="Фамилия" title="Введите фамилию" required /><br><br>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" name="first_name" style="width:300px; padding-left: 9px;" placeholder="Имя" title="Введите имя" required /><br><br>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" name="password" style="width:300px; padding-left: 9px;" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Не менее восьми символов, содержащих хотя бы одну цифру и символы из верхнего и нижнего регистра" placeholder="Пароль" required /><br><br>
				</td>
			</tr>
			<tr>
				<td>
					<input type="text" name="password_2" style="width:300px; padding-left: 9px;" placeholder="Повторите пароль" required /><br><br>
				</td>
			</tr>
			<tr>
				<td>
					<input class="button1" type="submit" value="Зарегистрироваться">
					<br>
				</td>
			</tr>
			<tr>
				<td>
					<div style="width:300px;" id="message">
					</div>
				</td>
			</tr>
		</table>
	</div>
</form>


