<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php echo $title; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<script src="/js/jquery.min.js"></script>
<script src="/js/main.js"></script>
<!-- Для листалки фоток -->
<link rel="stylesheet" type="text/css" href="/css/global.css" />
<!-- <script src="/js/jquery-1.4.4.min.js"></script> -->
<script src="/js/slides.min.jquery.js"></script>

<?php echo $javascript; ?>
</head>
<body class="body">
	<div class="site_background">
		<div class="site_container">
			
			<div class="head">
				<div class="head_1">
				</div>
				
				<div class="head_2">
					<h1>
						<a href="/">
							<span>
								STARSY SHOP
							</span>
						</a>
					</h1>
				</div>
				
				<div class="head_3">
					<div class="head_3_1">
						<a class="main_menu" href="/">
							<p>
								Главная
							</p>
						</a>
						<a class="main_menu" href="/?do=list_advertisements">
							<p>
								Объявления
							</p>
						</a>
						<a class="main_menu" href="">
							<p>
								О нас
							</p>
						</a>
						<a class="main_menu" style="width: 245px;" href="">
							<p>
								Контакты
							</p>
						</a>
					</div>
				</div>
				
				
				
				<?php if(check_cookie()) : ?>
					<div class="LK">
						<h3><a href="#">Личный кабинет</a></h3>
						<ul>
							<li><a href="/?do=profile" target="_self">Мой профиль</a></li>
							<li><a href="/?do=profile_advertisements" target="_self">Мои объявления</a></li>
							<!-- <li><a href="#" target="_self">Мои сообщения</a></li> -->
							<li><a href="/?do=logout" target="_self">Выход</a></li> 
						</ul>
					</div>
				<?php else : ?>
					<div class="head_5">
						<div class="head_5_contain">
							<div id="registration" style="width: 240px; font: normal normal normal 18px/1.3em din-next-w01-light,din-next-w02-light,din-next-w10-light,sans-serif;">
								<a href="" class="button1" onclick="ajax_show('http://<?php echo $_SERVER['HTTP_HOST'];?>/ajax.php?s=enter_form',400,285); return false;">Вход / Регистрация</a>
							</div>
						</div>
					</div>	
				<?php endif; ?>					
				
			</div>
			
			<div class="content">
				<?php echo $content; ?>
			</div>
			
			<!--
			<div class="bot">
				<div class="bot_name">
					<p class="font_9">
						<span style="color: #FFFFFF;">© 2015 Starsy Shop</span>
					</p>
				</div>
				<div class="instagram_icon">
					<a href=""><img src="../images/facebook_icon.png"></img></a>
				</div>
				<div class="facebook_icon">
					<a href=""><img src="../images/facebook_icon.png"></img></a>
				</div>
				<div class="vkontakte_icon">
					<a href=""><img src="../images/facebook_icon.png"></img></a>
				</div>
			</div>
			-->
			
		</div>
	</div>
	<div id="ajax_wrapper" style="display:none;">
		<div id="ajax_hide">
			<a href="#" onclick="ajax_hide();return false;"><img src="/images/ajax_hide.png"></a>
		</div>
		<div id="ajax_body">
		</div>
	</div>
</body>
</html>